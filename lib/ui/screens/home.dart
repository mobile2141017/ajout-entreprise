import 'package:add_entreprise/ui/views/list_view_company.dart';
import 'package:add_entreprise/ui/views/map_view_company.dart';
import 'package:flutter/material.dart';
import 'package:fluid_bottom_nav_bar/fluid_bottom_nav_bar.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  static int _index = 0;

  List<Widget> widgetList = [
    const ListViewCompany(),
    const MapViewCompany(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Liste des entreprises'),
      ),
/*       backgroundColor: const Color.fromARGB(255, 220, 217, 217), */
      body: widgetList[_index],
      bottomNavigationBar: FluidNavBar(
        onChange: (selectedIndex) => setState(() => _index = selectedIndex),
        icons: [
          FluidNavBarIcon(icon: Icons.card_travel_outlined),
          FluidNavBarIcon(icon: Icons.map_outlined),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => Navigator.pushNamed(context, '/add-company'),
        child: const Icon(Icons.add),
      ),
    );
  }
}
