import 'package:add_entreprise/blocs/company_cubit.dart';
import 'package:add_entreprise/models/company.dart';
import 'package:add_entreprise/models/adress.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:uuid/uuid.dart';

class AddCompany extends StatelessWidget {
  AddCompany({super.key});

  final GlobalKey<FormState> _formKey = GlobalKey();
  final TextEditingController _textFieldController = TextEditingController();
  final TextEditingController _adressController = TextEditingController();

  static Adress? _address;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Ajouter une entreprise'),
      ),
      body: BlocBuilder<CompanyCubit, List<Company>>(builder: (context, state) {
        return Container(
          margin: const EdgeInsets.all(10.0),
          child: Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                TextFormField(
                  controller: _textFieldController,
                  validator: (value) => value?.isEmpty == true
                      ? 'Le champs ne peut pas être vide'
                      : null,
                  decoration: const InputDecoration(
                      labelText: 'Nom de l\'entreprise',
                      icon: Icon(Icons.business),
                      border: OutlineInputBorder()),
                ),
                const SizedBox(height: 10),
                TextFormField(
                  controller: _adressController,
                  validator: (value) => value?.isEmpty == true
                      ? 'Le champs ne peut pas être vide'
                      : null,
                  onTap: () async {
                    final result =
                        await Navigator.pushNamed(context, '/search-adress')
                            as Adress?;
                    if (result != null) {
                      _address = result;
                      _adressController.text = _address!.toString();
                    }
                  },
                  readOnly: true,
                  decoration: const InputDecoration(
                    labelText: 'Adresse',
                    icon: Icon(Icons.location_on),
                    border: OutlineInputBorder(),
                  ),
                ),
                const SizedBox(height: 10),
                ElevatedButton(
                    onPressed: () {
                      if (_formKey.currentState!.validate() &&
                          _address != null) {
                        context.read<CompanyCubit>().addCompany(Company(
                            name: _textFieldController.text,
                            adress: _address!,
                            id: const Uuid().v8()));
                        Navigator.of(context).pop();
                      }
                    },
                    child: const Text('Valider')),
              ],
            ),
          ),
        );
      }),
    );
  }
}
