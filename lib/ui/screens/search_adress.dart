import 'package:add_entreprise/blocs/list_adress_cubit.dart';
import 'package:add_entreprise/models/adress.dart';
import 'package:add_entreprise/repositories/address_repository.dart';
import 'package:easy_debounce/easy_debounce.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SearchAdress extends StatelessWidget {
  const SearchAdress({super.key});

  static List<Adress>? _address;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Rechercher une adresse'),
      ),
      body:
          BlocBuilder<ListAdressCubit, List<Adress>>(builder: (context, state) {
        return Column(
          children: <Widget>[
            TextField(
              onChanged: (value) async {
                if (value.length < 3) return;
                EasyDebounce.debounce(
                    'fetch-debounce', const Duration(milliseconds: 500),
                    () async {
                  _address = await AddressRepository().fetchAddresses(value);
                  if (context.mounted && _address != null) {
                    context.read<ListAdressCubit>().setAdress(_address!);
                  }
                });
              },
              decoration: const InputDecoration(
                labelText: 'Rechercher une adresse',
                icon: Icon(Icons.search),
              ),
            ),
            Expanded(
              child: ListView(
                children: state
                    .map(
                      (option) => ListTile(
                        title: Text(option.toString()),
                        onTap: () => Navigator.pop(context, option),
                      ),
                    )
                    .toList(),
              ),
            ),
          ],
        );
      }),
    );
  }
}
