import 'package:add_entreprise/blocs/company_cubit.dart';
import 'package:add_entreprise/models/company.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ListViewCompany extends StatelessWidget {
  const ListViewCompany({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CompanyCubit, List<Company>>(
      builder: (context, companies) {
        return ListView.separated(
          separatorBuilder: (context, index) => const Divider(),
          itemCount: companies.length,
          itemBuilder: (context, index) {
            final Company company = companies[index];
            return ListTile(
              onTap: () => context.read<CompanyCubit>().removeCompany(company),
              title: Text(company.name),
              leading: const Icon(Icons.business),
              subtitle: Text(company.adress.toString()),
            );
          },
        );
      },
    );
  }
}
