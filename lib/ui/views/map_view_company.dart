import 'package:add_entreprise/blocs/company_cubit.dart';
import 'package:add_entreprise/models/company.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';

class MapViewCompany extends StatelessWidget {
  const MapViewCompany({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CompanyCubit, List<Company>>(
      builder: (context, companies) {
        LatLng getCenter(List<Company> companies) {
          if (companies.isEmpty) {
            return const LatLng(48.8566, 2.3522);
          }
          final double lat = companies
                  .map((company) => company.adress.latLng.latitude)
                  .reduce((value, element) => value + element) /
              companies.length;
          final double lng = companies
                  .map((company) => company.adress.latLng.longitude)
                  .reduce((value, element) => value + element) /
              companies.length;
          return LatLng(lat, lng);
        }

        return FlutterMap(
          options: MapOptions(initialCenter: getCenter(companies)),
          children: [
            TileLayer(
              urlTemplate: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
              userAgentPackageName: 'com.example.app',
            ),
            MarkerLayer(
                markers: companies
                    .map((company) => Marker(
                          width: 80.0,
                          height: 80.0,
                          point: company.adress.latLng,
                          child: const Icon(Icons.pin_drop, color: Colors.red),
                        ))
                    .toList())
          ],
        );
      },
    );
  }
}
