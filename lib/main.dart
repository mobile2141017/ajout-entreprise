import 'package:add_entreprise/blocs/company_cubit.dart';
import 'package:add_entreprise/blocs/list_adress_cubit.dart';
import 'package:add_entreprise/repositories/preferences_repository.dart';
import 'package:add_entreprise/ui/screens/add_company.dart';
import 'package:add_entreprise/ui/screens/home.dart';
import 'package:add_entreprise/ui/screens/search_adress.dart';
import 'package:flutter/material.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  final CompanyCubit companyCubit = CompanyCubit(PreferencesRepository());
  final ListAdressCubit listAdressCubit = ListAdressCubit();

  companyCubit.loadCompanies();

  runApp(MultiProvider(
    providers: [
      BlocProvider<CompanyCubit>(create: (_) => companyCubit),
      BlocProvider<ListAdressCubit>(create: (_) => listAdressCubit),
    ],
    child: const MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'App add entreprise',
      builder: BotToastInit(),
      navigatorObservers: [BotToastNavigatorObserver()],
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const Home(),
      routes: {
        '/add-company': (context) => AddCompany(),
        '/home': (context) => const Home(),
        '/search-adress': (context) => const SearchAdress(),
      },
    );
  }
}
