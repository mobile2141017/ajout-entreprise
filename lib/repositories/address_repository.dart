import 'dart:convert';
import 'package:add_entreprise/models/adress.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:bot_toast/bot_toast.dart';

class AddressRepository {
  Future<List<Adress>> fetchAddresses(String query) async {
    final Response response = await get(
        Uri.parse('https://api-adresse.data.gouv.fr/search?q=$query'));
    if (response.statusCode == 200) {
      final List<Adress> addresses = [];
      final Map<String, dynamic> json = jsonDecode(response.body);
      if (json.containsKey("features")) {
        final List<dynamic> features = json['features'];
        for (Map<String, dynamic> feature in features) {
          final Adress address = Adress.fromGeoJson(feature);
          addresses.add(address);
        }
      }
      return addresses;
    } else {
      BotToast.showText(
        contentColor: Colors.red,
        text: 'Erreur lors de la récupération des adresses',
      );
      return [];
    }
  }
}
