import 'dart:convert';

import 'package:add_entreprise/models/company.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PreferencesRepository {
  Future<void> saveCompanies(List<Company> companies) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final List<String> listJson = [];
    for (final Company company in companies) {
      listJson.add(jsonEncode(company.toJson()));
    }
    prefs.setStringList('companies', listJson);
  }

  Future<List<Company>> loadCompanies() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final List<String> listJson = prefs.getStringList('companies') ?? [];
    return listJson
        .map((String json) => Company.fromJson(jsonDecode(json)))
        .toList();
  }

  Future<List<Company>> removeCompany(Company company) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final List<String> listJson = prefs.getStringList('companies') ?? [];
    final updatedListJson = listJson.where((json) {
      final decodedJson = jsonDecode(json);
      final decodedCompany = Company.fromJson(decodedJson);
      return decodedCompany.id != company.id;
    }).toList();
    prefs.setStringList('companies', updatedListJson);
    return updatedListJson
        .map((String json) => Company.fromJson(jsonDecode(json)))
        .toList();
  }
}
