import 'package:latlong2/latlong.dart';

class Adress {
  final String street;
  final String city;
  final String postcode;
  final LatLng latLng;

  factory Adress.fromGeoJson(Map<String, dynamic> json) {
    final Map<String, dynamic> properties = json['properties'] ?? {};
    final String street = properties['name'];
    final String city = properties['city'];
    final String postcode = properties['postcode'];
    final List<dynamic> coordinates = json['geometry']['coordinates'];
    return Adress(
        street: street,
        city: city,
        postcode: postcode,
        latLng: LatLng(coordinates[1], coordinates[0]));
  }

  @override
  String toString() {
    return '$street, $city, $postcode';
  }

  Map<String, dynamic> toJson() {
    return {
      'street': street,
      'city': city,
      'postcode': postcode,
      'lat': latLng.latitude,
      'lng': latLng.longitude
    };
  }

  factory Adress.fromJson(Map<String, dynamic> json) {
    return Adress(
        street: json['street'],
        city: json['city'],
        postcode: json['postcode'],
        latLng: LatLng(json['lat'], json['lng']));
  }

  const Adress(
      {required this.street,
      required this.city,
      required this.postcode,
      required this.latLng});
}
