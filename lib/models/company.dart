import 'package:add_entreprise/models/adress.dart';

class Company {
  final String name;
  final Adress adress;
  final String id;

  Map<String, dynamic> toJson() {
    return {'name': name, 'adress': adress.toJson(), 'id': id};
  }

  factory Company.fromJson(Map<String, dynamic> json) {
    return Company(
        name: json['name'],
        adress: Adress.fromJson(json['adress']),
        id: json['id']);
  }

  Company({required this.name, required this.adress, required this.id});
}
