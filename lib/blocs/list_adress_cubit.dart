import 'package:add_entreprise/models/adress.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ListAdressCubit extends Cubit<List<Adress>> {
  ListAdressCubit() : super([]);

  Future<void> loadAdress() async {
    emit([]);
  }

  void setAdress(List<Adress> adress) {
    emit(adress);
  }
}
