import 'package:add_entreprise/models/company.dart';
import 'package:add_entreprise/repositories/preferences_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CompanyCubit extends Cubit<List<Company>> {
  final PreferencesRepository preferencesRepository;

  CompanyCubit(this.preferencesRepository) : super([]);

  Future<void> loadCompanies() async {
    final List<Company> companies = await preferencesRepository.loadCompanies();
    emit(companies);
  }

  void addCompany(Company company) {
    emit([...state, company]);
    preferencesRepository.saveCompanies(state);
  }

  Future<void> removeCompany(Company company) async {
    final List<Company> companies =
        await preferencesRepository.removeCompany(company);
    emit(companies);
  }
}
